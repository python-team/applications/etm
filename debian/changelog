etm (3.2.39-1) unstable; urgency=medium

  * Team upload
  * Drop transitional package etm-qt
  * New upstream version 3.2.39 (Closes: #1022016)
  * Depend on ruamel.yaml instead of pyyaml
  * Drop unused Expat
  * Use new AppStream format (Closes: #843683)

 -- Bastian Germann <bage@debian.org>  Sun, 22 Oct 2023 20:22:47 +0200

etm (3.2.30-4) unstable; urgency=medium

  [ Debian Janitor ]
  * Bump debhelper from old 12 to 13.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Sun, 16 Oct 2022 18:11:19 +0100

etm (3.2.30-3) unstable; urgency=medium

  [ Debian Janitor ]
  * Remove constraints unnecessary since buster:
    + etm: Drop versioned constraint on python3-dateutil, python3-icalendar and
      python3-yaml in Depends.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Wed, 25 May 2022 20:32:41 +0100

etm (3.2.30-2) unstable; urgency=medium

  [ Ondřej Nový ]
  * d/copyright: Use https protocol in Format field
  * d/control: Deprecating priority extra as per policy 4.0.1
  * d/watch: Use https protocol
  * d/control: Remove trailing whitespaces
  * Use debhelper-compat instead of debian/compat.
  * d/control: Update Maintainer field with new Debian Python Team
    contact address.
  * d/control: Update Vcs-* fields with new Debian Python Team Salsa
    layout.

  [ Debian Janitor ]
  * Use secure URI in Homepage field.
  * Bump debhelper from old 9 to 12.

 -- Sandro Tosi <morph@debian.org>  Sun, 17 Apr 2022 23:33:24 -0400

etm (3.2.30-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Tue, 05 Jan 2021 23:21:39 +0100

etm (3.2.30-1) unstable; urgency=medium

  * New upstream release
  * debian/control: Bump standards-verson to 3.9.8, no changes

 -- Jackson Doak <noskcaj@ubuntu.com>  Mon, 05 Jun 2017 11:06:03 +1000

etm (3.2.27-1) unstable; urgency=medium

  * New upstream release
  * debian/control: Bump standards-verson to 3.9.7, no changes

 -- Jackson Doak <noskcaj@ubuntu.com>  Sat, 04 Jun 2016 15:17:41 +1000

etm (3.2.22-1) unstable; urgency=medium

  * New upstream release
  * Drop debian/etm.menu, following CTTE #741573.
  * debian/rules: Remove duplicate changes file

 -- Jackson Doak <noskcaj@ubuntu.com>  Sun, 20 Dec 2015 08:05:08 +1100

etm (3.1.45-1) unstable; urgency=medium

  * New upstream release

 -- Jackson Doak <noskcaj@ubuntu.com>  Thu, 30 Jul 2015 06:52:18 +1000

etm (3.1.33-1) unstable; urgency=medium

  * New upstream release

 -- Jackson Doak <noskcaj@ubuntu.com>  Thu, 16 Jul 2015 06:03:55 +1000

etm (3.1.27-1) unstable; urgency=medium

  * New upstream release

 -- Jackson Doak <noskcaj@ubuntu.com>  Sat, 04 Jul 2015 07:09:16 +1000

etm (3.1.18-1) unstable; urgency=medium

  * New upstream release
  * Update d/watch

 -- Jackson Doak <noskcaj@ubuntu.com>  Mon, 22 Jun 2015 06:11:10 +1000

etm (3.0.43-1) unstable; urgency=medium

  * New upstream release

 -- Jackson Doak <noskcaj@ubuntu.com>  Tue, 17 Mar 2015 06:23:27 +1100

etm (3.0.42-1) unstable; urgency=medium

  * New upstream release

 -- Jackson Doak <noskcaj@ubuntu.com>  Sun, 14 Dec 2014 08:37:33 +1100

etm (3.0.41-1) unstable; urgency=medium

  * New upstream release. Closes: #764742
    - Drop patch, applied upstream
  * debian/control: Change section to "utils". Closes: #764290

 -- Jackson Doak <noskcaj@ubuntu.com>  Sat, 11 Oct 2014 13:11:24 +1100

etm (3.0.40-1) unstable; urgency=medium

  * Initial release. This package replaces etm-qt. Closes: #754261

 -- Jackson Doak <noskcaj@ubuntu.com>  Wed, 01 Oct 2014 08:23:35 +1000
